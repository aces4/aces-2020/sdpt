#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <drivers/System.hpp>

class MockCycleCounterDriver : public CycleCounterDriverBase {
public:
    MOCK_METHOD(uint64_t, getCycleCount, (), (override));
};

class SystemTest : public ::testing::Test {
protected:
    MockCycleCounterDriver driver;
    System *mSystem;

    void SetUp() override {
        mSystem = new System(driver);
    }

    void TearDown() override {
        delete mSystem;
    }
};

TEST_F(SystemTest, TestFrequencyComputer) {
    EXPECT_CALL(driver, getCycleCount()).Times(::testing::Exactly(3)).WillOnce(::testing::Return(0))
            .WillOnce(::testing::Return(100000000)).WillRepeatedly(::testing::Return(-1));
    ASSERT_EQ(mSystem->computeClockFrequencyMHz(), 100.0);
}
