#include <gtest/gtest.h>
#include "RadusArabToRomanConverter.hpp"
#include "JohnsArabToRomanConverter.hpp"
#include <iostream>

class ArabToRomanConverterTest: public ::testing::Test, public ::testing::WithParamInterface<ArabToRomanConverter*> {
public:
    ArabToRomanConverter * mConverter;
    
    static void SetUpTestCase() {
        
    }
    
    static void TearDownTestCase() {
        // delete mConverter;
    }
    
    void SetUp() override {
        mConverter = GetParam();
    }
    
    void TearDown() override {
//         std::cout << "Calling tearDown!" << std::endl;
    }
};

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor0) {
    ASSERT_THROW(mConverter->convert(0), std::invalid_argument);
}

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor1) {
    ASSERT_EQ(mConverter->convert(1), "I"); // => mConverter->convert(1) == "I"
}

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor2) {
    ASSERT_EQ(mConverter->convert(2), "II");
}

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor3) {
    ASSERT_EQ(mConverter->convert(3), "III");
}

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor4) {
    ASSERT_EQ(mConverter->convert(4), "IV");
}

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor7) {
    ASSERT_EQ(mConverter->convert(7), "VII");
}

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor9) {
    ASSERT_EQ(mConverter->convert(9), "IX");
}

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor11) {
    ASSERT_EQ(mConverter->convert(11), "XI");
}

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor34) {
    ASSERT_EQ(mConverter->convert(34), "XXXIV");
}

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor29) {
    ASSERT_EQ(mConverter->convert(29), "XXIX");
}

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor38) {
    ASSERT_EQ(mConverter->convert(38), "XXXVIII");
}

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor49) {
    ASSERT_EQ(mConverter->convert(49), "XLIX");
}

TEST_P(ArabToRomanConverterTest, TestsFunctionalityFor150) {
    EXPECT_EQ(mConverter->convert(150), "CL");
    std::cout << "OMG this test failed, I'm gonna get fired!!" << std::endl;
}

INSTANTIATE_TEST_SUITE_P(ArabToNumeralConverterParameterizedTestSuite,
                         ArabToRomanConverterTest,
                         testing::Values(new RadusArabToRomanConverter(), new JohnsArabToRomanConverter()));
