/** ****************************************************************************
 * @file System
 * @author Radu Hobincu
 * @date [April 19, 2021]
 * @brief Header Source file for the System class.
 *
 * @copyright Copyright 2014-2021 Silicon Acuity. All rights reserved. Do not
 *      distribute or modify without permission.
 ******************************************************************************/

#pragma once


#include "CycleCounterDriverBase.hpp"

class System {
    CycleCounterDriverBase & mDriver;
public:
    explicit System(CycleCounterDriverBase & driver);
    double computeClockFrequencyMHz();
};



