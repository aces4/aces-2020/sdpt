/** ****************************************************************************
 * @file CycleCounterDriver
 * @author Radu Hobincu
 * @date [April 19, 2021]
 * @brief Header Source file for the CycleCounterDriver class.
 *
 * @copyright Copyright 2014-2021 Silicon Acuity. All rights reserved. Do not
 *      distribute or modify without permission.
 ******************************************************************************/

#include "CycleCounterDriver.hpp"

CycleCounterDriver::CycleCounterDriver(const std::string &path) : mPath(path), mInputFile(path){
    if(!mInputFile.is_open()) {
        throw std::invalid_argument("Can't open file " + path);
    }
}

uint64_t CycleCounterDriver::getCycleCount() {
    uint64_t cycleCount;
    mInputFile >> cycleCount;
    return cycleCount;
}
