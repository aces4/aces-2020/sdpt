/** ****************************************************************************
 * @file CycleCounterDriver
 * @author Radu Hobincu
 * @date [April 19, 2021]
 * @brief Header Source file for the CycleCounterDriver class.
 *
 * @copyright Copyright 2014-2021 Silicon Acuity. All rights reserved. Do not
 *      distribute or modify without permission.
 ******************************************************************************/

#pragma once

#include <string>
#include <fstream>
#include "CycleCounterDriverBase.hpp"

class CycleCounterDriver : public CycleCounterDriverBase{
    std::string mPath;
    std::ifstream mInputFile;
public:
    explicit CycleCounterDriver(const std::string & path);
    uint64_t getCycleCount() override;
};



