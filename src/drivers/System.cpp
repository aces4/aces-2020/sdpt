/** ****************************************************************************
 * @file System
 * @author Radu Hobincu
 * @date [April 19, 2021]
 * @brief Header Source file for the System class.
 *
 * @copyright Copyright 2014-2021 Silicon Acuity. All rights reserved. Do not
 *      distribute or modify without permission.
 ******************************************************************************/

#include <iostream>
#include <unistd.h>
#include "System.hpp"

System::System(CycleCounterDriverBase &driver) : mDriver(driver){

}

double System::computeClockFrequencyMHz() {
    uint64_t startCycle = mDriver.getCycleCount();
    usleep(1000000);
    uint64_t endCycle = mDriver.getCycleCount();
    return (endCycle - startCycle) / 1000000.0;
}
