#include <iostream>
#include "CycleCounterDriver.hpp"
#include "System.hpp"

int main() {
    int * array = new int[100];
    std::cout << "Array starts at address " << array << " and the array variable is stored at " << &array << std::endl;
    free(array);

    try {
        CycleCounterDriver driver("clock_input_file.txt");
        std::cout << "Current cycle count is: " << driver.getCycleCount() << std::endl;
        System system(driver);
        std::cout << "Clock Frequency is: " << system.computeClockFrequencyMHz() << " MHz" << std::endl;
    }catch(std::exception & ex) {
        std::cout << "Exception: " << ex.what() << std::endl;
    }
    return 0;
}
