#pragma once


struct CycleCounterDriverBase {
    virtual uint64_t getCycleCount() = 0;
    virtual ~CycleCounterDriverBase() = default;
};



