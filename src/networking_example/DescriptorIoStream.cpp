/** ****************************************************************************
 * @file DescriptorIoStream
 * @author Radu Hobincu
 * @date [May 10, 2021]
 * @brief Header Source file for the DescriptorIoStream class.
 *
 * @copyright Copyright 2014-2021 Silicon Acuity. All rights reserved. Do not
 *      distribute or modify without permission.
 ******************************************************************************/

#include "DescriptorIoStream.hpp"

DescriptorIoStream::DescriptorIoStream(int descriptor) : mDescriptor(descriptor) {

}

DescriptorIoStream::DescriptorIoStream(const DescriptorIoStream &original) : mDescriptor(original.mDescriptor) {

}

void DescriptorIoStream::sendBytes(const std::vector<uint8_t> &buffer) {
    ssize_t bytesWritten = write(mDescriptor, buffer.data(), buffer.size());

    if (bytesWritten < 0) {
        throw std::runtime_error("Port would not write!");
    }
}

std::vector<uint8_t> DescriptorIoStream::receiveBytes(uint16_t maxByteCount, uint32_t timeout) {
    fd_set set;
    timeval timeoutStruct{};
    int32_t bytesAvailable;

    FD_ZERO(&set); /* clear the set */
    FD_SET(mDescriptor, &set); /* add our file descriptor to the set */

    timeoutStruct.tv_sec = timeout / 1000;
    timeoutStruct.tv_usec = timeout % 1000 * 1000;

    bytesAvailable = select(mDescriptor + 1, &set, nullptr, nullptr, &timeoutStruct);
    if (bytesAvailable == -1) {
        throw std::runtime_error("Port would not give state");
    }

    if (bytesAvailable == 0) {
        return std::vector<uint8_t>();
    }

    std::vector<uint8_t> buffer(maxByteCount, 0);
    ssize_t actualNumOfBytes = read(mDescriptor, buffer.data(), maxByteCount);
    buffer.resize(static_cast<uint64_t>(actualNumOfBytes));
    return buffer;
}

std::vector<uint8_t> DescriptorIoStream::receiveBytes(uint16_t maxByteCount) {
    std::vector<uint8_t> buffer(maxByteCount);

    ssize_t actualNumOfBytes = read(mDescriptor, buffer.data(), maxByteCount);
    if (actualNumOfBytes <= 0) {
        throw std::runtime_error("Port would not read");
    }
    buffer.resize(actualNumOfBytes);

    return buffer;

}

std::vector<uint8_t> DescriptorIoStream::receiveBytesExactly(uint16_t maxByteCount) {
    std::vector<uint8_t> buffer(maxByteCount);
    uint16_t bytesRead = 0;

    while (bytesRead != maxByteCount) {
        ssize_t actualNumOfBytes = read(mDescriptor, buffer.data() + bytesRead, maxByteCount - bytesRead);
        if (actualNumOfBytes <= 0) {
            throw std::runtime_error("Port would not read");
        }
        bytesRead += actualNumOfBytes;
    }
    return buffer;

}

uint8_t DescriptorIoStream::readByte() {
    uint8_t result;
    if (read(mDescriptor, &result, 1) != 1) {
        throw std::runtime_error("Could not read byte");
    }
    return result;
}

void DescriptorIoStream::finish() {
    close(mDescriptor);
}

void DescriptorIoStream::sendString(const std::string &buffer) {
    ssize_t bytesWritten = write(mDescriptor, buffer.data(), buffer.size());

    if (bytesWritten < 0) {
        throw std::runtime_error("Port would not write!");
    }
}

std::string DescriptorIoStream::readString(uint16_t maxCharCount) {
    std::string buffer;
    buffer.resize(maxCharCount + 1);
    uint16_t bytesRead = 0;

    while (bytesRead != maxCharCount) {
        ssize_t actualNumOfBytes = read(mDescriptor, (void *) (buffer.data() + bytesRead), maxCharCount - bytesRead);
        if (actualNumOfBytes <= 0) {
            throw std::runtime_error("Port would not read");
        }
        bytesRead += actualNumOfBytes;
    }
    buffer.back() = '\0';
    return buffer;
}
