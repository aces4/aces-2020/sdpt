/** ****************************************************************************
 * @file DescriptorIoStream
 * @author Radu Hobincu
 * @date [May 10, 2021]
 * @brief Header Source file for the DescriptorIoStream class.
 *
 * @copyright Copyright 2014-2021 Silicon Acuity. All rights reserved. Do not
 *      distribute or modify without permission.
 ******************************************************************************/

#pragma once

#include <vector>
#include <cstdint>
#include <cstdlib>
#include <unistd.h>
#include <stdexcept>

class DescriptorIoStream {
protected:
    const int mDescriptor;

    explicit DescriptorIoStream(int descriptor);

    DescriptorIoStream(const DescriptorIoStream &original);

public:
    /**
     * Writes a string message to the stream.
     * @param message the message to write.
     */
    virtual void sendBytes(const std::vector<uint8_t> &buffer);

    /**
     * Writes a string message to the stream.
     * @param message the message to write.
     */
    virtual void sendString(const std::string &buffer);

    /**
     * Reads a message from the stream.
     * @return the message read from the stream.
     */
    virtual std::vector<uint8_t> receiveBytes(uint16_t maxByteCount, uint32_t timeout);

    /**
     * Reads a message from the stream.
     * @return the message read from the stream.
     */
    virtual std::vector<uint8_t> receiveBytes(uint16_t maxByteCount);

    /**
     * Reads a message from the stream.
     * @return the message read from the stream.
     */
    virtual std::vector<uint8_t> receiveBytesExactly(uint16_t maxByteCount);

    /**
     * Reads a message from the stream.
     * @return the message read from the stream.
     */
    virtual std::string readString(uint16_t maxCharCount);

    virtual uint8_t readByte();

    virtual void finish();

    virtual ~DescriptorIoStream() = default;
};



