/** ****************************************************************************
 * @file TcpSocket
 * @author Radu Hobincu
 * @date [May 10, 2021]
 * @brief Header Source file for the TcpSocket class.
 *
 * @copyright Copyright 2014-2021 Silicon Acuity. All rights reserved. Do not
 *      distribute or modify without permission.
 ******************************************************************************/

#include <netdb.h>
#include <cstring>
#include "TcpSocket.hpp"

TcpSocket::TcpSocket(const std::string &serverName, uint16_t port) : DescriptorIoStream(
        createDescriptor(serverName, port)) {

}

int TcpSocket::createDescriptor(const std::string &serverName, uint16_t port) {
    int socketFileDescriptor;
    sockaddr_in serverAddress{};
    struct hostent *server;

    socketFileDescriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socketFileDescriptor < 0) throw std::runtime_error("Unable to initialize socket.");
    server = gethostbyname(serverName.c_str());
    if (server == nullptr) throw std::runtime_error("Can't resolve " + serverName);

    serverAddress.sin_family = AF_INET;
    memmove((char *) &serverAddress.sin_addr.s_addr, (char *) server->h_addr, server->h_length);
    serverAddress.sin_port = htons(port);
    if (connect(socketFileDescriptor, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0) {
        throw std::runtime_error("Error connecting...");
    }
    return socketFileDescriptor;
}
