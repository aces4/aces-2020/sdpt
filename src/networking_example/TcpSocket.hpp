/** ****************************************************************************
 * @file TcpSocket
 * @author Radu Hobincu
 * @date [May 10, 2021]
 * @brief Header Source file for the TcpSocket class.
 *
 * @copyright Copyright 2014-2021 Silicon Acuity. All rights reserved. Do not
 *      distribute or modify without permission.
 ******************************************************************************/

#pragma once


#include "DescriptorIoStream.hpp"

class TcpSocket : public DescriptorIoStream {

    static int createDescriptor(const std::string & serverIp, uint16_t port);

public:

    TcpSocket(const TcpSocket &original) = delete;

    TcpSocket(const std::string & serverName, uint16_t port);

    /**
     * Class destructor. Closes the socket and releases all associated resources.
     */
    ~TcpSocket() override = default;
};



