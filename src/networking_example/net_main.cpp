#include <iostream>
#include "TcpSocket.hpp"

/** ****************************************************************************
 * @file net_main
 * @author Radu Hobincu
 * @date [May 10, 2021]
 * @brief Header Source file for the net_main class.
 *
 * @copyright Copyright 2014-2021 Silicon Acuity. All rights reserved. Do not
 *      distribute or modify without permission.
 ******************************************************************************/

int main() {
    try {
        TcpSocket socket("localhost", 8000);
        socket.sendBytes({0x05, 0x00, 0x11, 0x22, 0x32, 0xff});

    }catch(std::exception & ex) {
        std::cout << "Error caught: " << ex.what() << std::endl;
    }
}
