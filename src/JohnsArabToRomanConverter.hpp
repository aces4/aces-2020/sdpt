#pragma once

#include "ArabToRomanConverter.hpp"

/**
 * @brief John's implementation of the Arab to Roman converter. It is not very good.
 *
 * We can add here a lot more text. This is a lot more text.
 * It can use more than one line.
 * It can use <b>HTML</b> tags.
 */
struct JohnsArabToRomanConverter : public ArabToRomanConverter {
    /**
     * @brief Converts a strictly positive number to its Roman representation.
     * @param value The value to convert.
     * @return a string that represents the Roman sequence.
     * @throws std::invalid_argument if the value is 0.
     */
    std::string convert(uint32_t value) override;
};
