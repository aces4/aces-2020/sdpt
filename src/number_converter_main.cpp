#include <iostream>
#include "RadusArabToRomanConverter.hpp"
/**
 * Program entry point, it will display a cute message.
 * @param argc The number of arguments provided when the program was started.
 * @param argv The argument array (argv[0] is usually the executable name).
 * @return an error code, where 0 means the execution was successful.
 */
int main(int argc, char ** argv) {
    int * array = new int[100];
    std::cout << "Array starts at address " << array << " and the array variable is stored at " << &array << std::endl;
    free(array);

    uint32_t value;
    std::cout << "Enter an unsigned number: ";
    std::cin >> value;

    RadusArabToRomanConverter converter;

    std::cout << "Roman numeral for " << value << " is " << converter.convert(value) << std::endl;
	return 0;
}
