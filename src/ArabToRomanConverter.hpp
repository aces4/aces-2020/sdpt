#pragma once

#include <string>
#include <cstdint>

struct ArabToRomanConverter {   
    virtual std::string convert(uint32_t value) = 0;
};
