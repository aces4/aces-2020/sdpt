#include <stdexcept>
#include "JohnsArabToRomanConverter.hpp"

std::string JohnsArabToRomanConverter::convert(uint32_t value) {
    if(value == 0) throw std::invalid_argument("Can't convert 0 to roman");
    return "I";
}
