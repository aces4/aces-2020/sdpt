/** ****************************************************************************
 * @file mmap_writer_main
 * @author Radu Hobincu
 * @date [May 10, 2021]
 * @brief Header Source file for the mmap_writer_main class.
 *
 ******************************************************************************/

#include <sys/mman.h>
#include <fcntl.h>
#include <iostream>
#include <cstring>
#include <unistd.h>

constexpr size_t SIZE = 16 * 1024*1024;

struct Contents{
    uint32_t written;
    uint32_t size;
    char contents[SIZE - 8];
};

int main() {
    std::string fileName = "data_sharing.mmap";

    int fd;
    if ((fd = open(fileName.c_str(), O_RDWR| O_CREAT, 0644)) == -1) {
        std::cout << "Unable to open " << fileName << std::endl;
        return 0;
    }

    ftruncate(fd, SIZE);

    // open the file in shared memory
    auto *shared = static_cast<Contents *> (mmap(nullptr, SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0));


    // periodically read the file contents
    char text[] = "Hello from C++!";

    strcpy(shared->contents, text);
    shared->size = strlen(text);
    shared->written = 1;

//    std::cout << "Syncing... " << std::endl;
//    msync(shared, SIZE, MS_SYNC);
//    std::cout << "Done!" << std::endl;



    while(shared->written == 1) usleep(10);

    getchar();

    munmap(shared, SIZE);

    close(fd);
    return 0;
}
