#!/usr/bin/env python3

import mmap
import os
import time
 
fname = 'data_sharing.mmap'
SIZE = 16 * 1024 * 1024

if not os.path.isfile(fname):
    print('File not found!\n')
    exit(0)

# at this point, file exists, so memory map it
with open(fname, "r+b") as fd:
    mm = mmap.mmap(fd.fileno(), SIZE, access=mmap.ACCESS_WRITE, offset=0)

    # set one of the pods to true (== 0x01) all the rest to false
    posn = 0
    while True:
        mm.seek(0)
        written = int.from_bytes(mm.read(4), "little")
        ##print(written)
        if written == 1: 
            break
    
    string_size = int.from_bytes(mm.read(4), "little")
    print(mm.read(string_size).decode('utf-8'), '\n')
    mm.seek(0)
 
    mm.write(b'\x00\x00\x00\x00')
 
    mm.close()
    fd.close()
