#pragma once

#include "ArabToRomanConverter.hpp"

struct RadusArabToRomanConverter : public ArabToRomanConverter {
    std::string convert(uint32_t value) override;
};
