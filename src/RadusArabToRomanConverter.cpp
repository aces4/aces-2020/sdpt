#include "RadusArabToRomanConverter.hpp"
#include <vector>
#include <stdexcept>

std::string RadusArabToRomanConverter::convert(uint32_t value) {
    if(value == 0) {
        throw std::invalid_argument("Value of 0 has no roman numeral correspondent.");
    }
    
    
    std::string result;
    std::vector<std::pair<uint32_t, std::string>> entities = {{40, "XL"}, {10, "X"}, {9, "IX"}, {5, "V"}, {4, "IV"}, {1, "I"}};
    
    for(const auto & p : entities) {
        while(value >= p.first) {
            result += p.second;
            value -= p.first;
        }
    }
        
    return result;
}
