CPP_FILES=$(shell find src/ -name "*.cpp")
OBJ_FILES=$(CPP_FILES:.cpp=.o)
TEST_CPP_FILES=$(shell find unit-tests/ -name "*.cpp")
TEST_OBJ_FILES=$(TEST_CPP_FILES:.cpp=.o)
D_FILES=$(CPP_FILES:.cpp=.d)
SYSTEM_EXE=system
# CXXFLAGS - flags for the C++ compiler, CFLAGS - flags for the C compiler, CXX - the C++ compiler
CXXFLAGS=-MMD --std=c++17 -O3 -g -Wall -Isrc/ --coverage
# LDFLAGS should contain linked libraries
LDFLAGS=--coverage

.PHONY: all
all: $(SYSTEM_EXE)


# $^ means ALL the source files and $< means only THE FIRST file in the list of source files
$(SYSTEM_EXE): $(filter-out src/number_converter_main.o,$(OBJ_FILES))
	$(CXX) $^ -o $@ $(LDFLAGS)

tests: $(filter-out src/number_converter_main.o src/drivers/driver_main.o src/networking_example/net_main.o src/shared_mmap_example/mmap_writer_main.o,$(OBJ_FILES)) $(TEST_OBJ_FILES)
	$(CXX) $^ -o $@ $(LDFLAGS) -lgtest -lgmock -pthread

# clean target does not generate a file named clean
.PHONY: clean
clean:
	rm -rf $(SYSTEM_EXE)
	find src -name "*.o" | xargs rm -f
	find src -name "*.d" | xargs rm -f
	find src -name "*.gcno" | xargs rm -f
	find src -name "*.gcda" | xargs rm -f

-include $(D_FILES)
